var express = require('express');
var bodyParser = require('body-parser');

var puertas = require('./routes/MIDBlynk_puertas');
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/',puertas);

var port = 3030;

app.listen(port,() => {
    console.log('servidor de suma funcionando '+port);
});
