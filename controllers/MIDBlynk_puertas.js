var Product = require('../models/MIDBlynk_puertas');
const request = require('request');

const endpoint_500 = 'http://blynk.thecloudcomputing.io:8080/f9bc63ec578d4d2a8a4d2bd1d93021e8/update/';
const endpoint_501 = 'http://blynk.thecloudcomputing.io:8080/03408abac86747a5bf0f6afe8dfe04f4/update/';
const endpoint_502 = 'http://blynk.thecloudcomputing.io:8080/7e39f6f2511641d5ac78223f6f6082e4/update/';
const endpoint_503 = 'http://blynk.thecloudcomputing.io:8080/b366d83f391d4f2c9e36e41aa1ef288e/update/';
const endpoint_504 = 'http://blynk.thecloudcomputing.io:8080/16b6559314c6461db2fc0a52dfa6812b/update/';
const endpoint_505 = 'http://blynk.thecloudcomputing.io:8080/cad2e5a4492840a5bd9d88e8e1d503e5/update/';
const endpoint_506 = 'http://blynk.thecloudcomputing.io:8080/732ce675060745a3ba0edc07d5b0d1bc/update/';
const endpoint_507 = 'http://blynk.thecloudcomputing.io:8080/09a123777e134e1c9010efb3f26a0097/update/';
const endpoint_508 = 'http://blynk.thecloudcomputing.io:8080/6a0308d3f751473184a1df19f8ddb32c/update/';
const endpoint_601 = 'http://blynk.thecloudcomputing.io:8080/e2e1d9138c7342bf9adf2b5b8d731838/update/';
const endpoint_701 = 'http://blynk.thecloudcomputing.io:8080/8c8203d4c3b54221980c047a174756c9/update/';
const endpoint_702 = 'http://blynk.thecloudcomputing.io:8080/842d6ea156b44cf7a50c0a65d377aa6f/update/';
const endpoint_703 = 'http://blynk.thecloudcomputing.io:8080/607bf847df6f4f6e8cbd6453db289168/update/';
const endpoint_704 = 'http://blynk.thecloudcomputing.io:8080/216645483370459cac19ffe3221dae1e/update/';
const endpoint_706 = 'http://blynk.thecloudcomputing.io:8080/236a147eb72546b4b031f2d906944566/update/';
const endpoint_707 = 'http://blynk.thecloudcomputing.io:8080/7239d74a33cf4fada24337e1d063c1ba/update/';



exports.puertas = function (req,res){

    var sala = req.body.sala;
    var accion = req.body.accion;
    var autor = req.body.autor; 

    if (sala == '500')
    {
        var url = endpoint_500+"V0"+"?value="+accion;
        request(url, function (error, response, body) {
            console.log(url);
            console.error('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
        });

        res.send({ status: "Enviado: "+sala });
    }


};
exports.status = function(req,res){
    res.send({ status: "OK" });
}